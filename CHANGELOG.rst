Changelog:
==========

Version 3.0.0
-------------

* Qt library ``PySide2`` updated to ``PySide6``

Version 2.2.2
-------------

* Change email adresses
* Prepare debian package support
* Bug fixes

Version 2.2.0
-------------

* Change build-system to ``Flit``
* Replace ``PySide6`` with ``PySide2`` to prepare debian package support
* Some code that required ``Python 3.10`` changed to ``Python 3.9``

Version 2.1.0
-------------

* UI redesign
* 'Lock crypted device if unlocked' temporarily removed
* Added stats about writing time and overall speed
* Added optional validation after writing
* Added simultaneous writing of an image to multiple USB sticks
* Changed adjustable write buffer between 4 KiB and 8 MiB
* Many code improvements and bug fixes

Version 2.0.1
-------------

* Code improvements

Version 2.0.0
-------------

* Remove ``usb-imager-git`` from Arch-Linux ``AUR`` repository
* python min version >= 3.9
* Replace ``PySide2`` with ``PySide6``
* Code improvements and bug fixes

Version 1.1
-----------

* ``usb-imager-git`` is now available in the Arch-Linux ``AUR`` repository
* Writing buffer values changed
* Improve error detection and error messages in case of missing dependencies
* Code improvements and minor bug fixes

Version 1.0
-----------

* Initial version
